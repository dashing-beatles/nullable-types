\section{Introduction}

De nombreux langages impératifs comme C, Java ou leurs dérivés utilisent
la constante \texttt{NULL}. Cette constante prend une sémantique différente
selon les utilisations, comme un résultat erroné ou une donnée inconnue.
C'est une valeur pratique: il suffit d'un simple test pour vérifier
si une valeur est \texttt{NULL} ou pas.

Un type nullable est un type dont les valeurs, en plus de ses valeurs usuelles,
comprennent cette valeur \texttt{NULL} représentant l'indéfini.
Par exemple, en C\#, une variable de type \texttt{int?} peut contenir toute valeur entière
encodable par un simple \texttt{int}, ainsi que \texttt{null}.

L'article \cite{mauny2014nullable} présente un système d'inférence de types nullables
à base de contraintes de sous-typage, qui s'appuie sur les travaux de thèse
(\cite{vaugon:tel-01356695}) de Benoît Vaugon.

Nous implémentons ce système en OCaml et todo{Finish introduction later}…

\section{Système de sous-typage}
L'approche proposée par \cite{mauny2014nullable}
pour typer les programmes contenant des types nullables est de
contraindre les types rencontrés à certaines relations
de compatibilité ou de sous-typage
au fur et à mesure et de vérifier à chaque nouvelle contrainte
qu'elle est compatible avec les antécédentes.

Nous présentons ici ce système de type.

\subsection{Formalismes utilisés}

\begin{definition}
  Le formalisme de types utilisé est le suivant.
  \begin{align*}
    t \coloneqq t_b \mid \alpha \mid \tau_1 \to \tau_2
    && \tau \coloneqq t \mid t?
  \end{align*}

  où $\alpha$ est un type inconnu, possiblement nullable,
  et $t_b$ est un type de base comme \texttt{int} ou \texttt{bool}.
\end{definition}

\begin{definition}
  $t?$ est le type qui ajoute au type $t$ la valeur \texttt{NULL}.
\end{definition}

\begin{definition}
  La relation de sous-typage est notée $\leqslant$ et $\leq$.

  $\leqslant$ et $\leq$ sont deux symboles utilisés pour définir la même
  relation. La distinction entre les deux symboles existe pour faciliter
  l'automatisation des preuves de sous-typage.
\end{definition}

\begin{definition}
  La relation de compatibilité est notée $\approx$ et $\simeq$.

  La compatibilité est symmétrique: $\tau_1 \approx \tau_2 \iff \tau_2 \approx \tau_1$.

  Similairement à $\leqslant$ et $\leq$, la distinction entre $\approx$ et
  $\simeq$ existe pour faciliter l'automatisation des preuves de compatibilité.
\end{definition}

\begin{definition}
  $\Phi$ est une notation utilisée pour représenter
  une suite de contraintes de la forme $\tau_1\, \mathcal{R}\, \tau_2$ où
  $\mathcal{R} \in \{\leqslant, \approx\}$.
\end{definition}

\begin{definition}
  $\Gamma$ est une notation utilisée pour représenter
  un environnement de typage qui à un identifiant $x$ associe
  un schéma de type $\sigma_x$.
\end{definition}

\begin{definition}
  Un schéma de type $\sigma$ est une conjonction d'un type quantifié
  et d'un ensemple de contraintes $\Phi$. La forme générale est
  $$\sigma = \forall \alpha_1 \dotsb \alpha_n . \tau \mid \Phi$$
  où $\Phi$ ne contient que des contraintes relatives à un des $\alpha_1^n$.
\end{definition}

\begin{definition}
  Un jugement de typage est de la forme
  $$\Phi, \Gamma \vdash e : \tau \rhd \Phi'
  \quad\text{où } e \text{ est une expression à typer}
  $$
  Il se lit: \og \textit{Selon les contraintes de $\Phi$ et l'environnement $\Gamma$,
  on peut typer $e$ par $\tau$, accumulant toute nouvelle contrainte dans $\Phi'$ }\fg{}
\end{definition}

\begin{definition}
  Un jugement de relation de types est de la forme
  $$\Phi \vdash \tau_1\, \mathcal{R}\, \tau_2 \rhd \Phi'
  \quad\text{où }\mathcal{R} \in
  \{\leq,\leqslant,\approx,\simeq\}
  $$
  Il se lit \og \textit{Selon les contraintes de $\Phi$,
    on peut prouver $\tau_1\, \mathcal{R}\, \tau_2$
    accumulant toute nouvelle contrainte dans $\Phi'$}\fg{}
\end{definition}

\begin{definition}
  On note \og$\alpha$ fresh\fg{} la création d'une variable nouvelle $\alpha$
  avec un nom unique.
\end{definition}

\subsection{Règles de sous-typage}

Ici on présente les règles de sous-typage. Ce ne sont pas des règles de typage
parce qu'elles n'attribuent pas à l'expression un type défini, elles
s'assurent simplement que l'expression sous-type le type $\ta$ qu'on lui attribue.
Ces règles sont définies en figure \ref{fig:rulesty}.

\begin{figure}
  \centering
  \begin{mdframed}
    \begin{subfigure}{0.5\linewidth}
      \begin{topprooftree}{\textsc{TConst}}
        \AXC{$\Phi \fCenter T(c) \leqslant \tau \rhd \Phi'$}
        \UIC{$ \Phi, \Gamma}
        \fCenter c : \tau \rhd \Phi'$
      \end{topprooftree}
    \end{subfigure}
    ~
    \begin{subfigure}{.5\linewidth}
      \begin{topprooftree}{\textsc{TNull}}
        \AXC{$\alpha$ fresh}
        \AXC{$\Phi \vdash \alpha? \leqslant \tau \rhd \Phi'$}
        \BIC{$\Phi, \Gamma \vdash \texttt{NULL} : \tau \rhd \Phi'$}
      \end{topprooftree}
    \end{subfigure}

    \begin{topprooftree}{\textsc{TInst}}
      \AXC{$\alpha_1' \dotsb \alpha_n'$ fresh}
      \AXC{$\Phi, C_1[\alpha_k \to \alpha_k']_1^n,
        \dotsb, C_p[\alpha_k \to \alpha_k']_1^n
        \vdash \tau[\alpha_k \to
        \alpha_k']_1^n \leqslant \tau'
        \rhd \Phi'$}
      \BIC{$\Phi, \Gamma \oplus x :
        [\forall \alpha_1 \dotsb \alpha_n . \tau \mid C_1 \wedge
        \dotsb \wedge C_p]
        \vdash x : \tau'
        \rhd \Phi'$}
    \end{topprooftree}

    \begin{topprooftree}{\textsc{TLambda}}
      \AXC{$\alpha_1, \alpha_2$ fresh}
      \AXC{$\Phi, \Gamma \oplus x : \alpha_1 \vdash e : \alpha_2 \rhd \Phi'$}
      \AXC{$\Phi' \vdash \alpha_1 \to \alpha_2 \leqslant \tau \rhd \Phi''$}
      \TIC{$\Phi, \Gamma
        \vdash \texttt{function } x \texttt{ -> } e : \tau
        \rhd \Phi''$}
    \end{topprooftree}

    \begin{topprooftree}{\textsc{TApp}}
      \AXC{$\alpha$ fresh}
      \AXC{$\Phi, \Gamma \vdash e_1 : \alpha \to \tau \rhd \Phi'$}
      \AXC{$\Phi', \Gamma \vdash e_2 : \alpha \rhd \Phi''$}
      \TIC{$\Phi, \Gamma
        \vdash e_1 e_2 : \tau
        \rhd \Phi''$}
    \end{topprooftree}

    \begin{topprooftree}{\textsc{TIfThenElse}}
      \AXC{$\Phi, \Gamma \vdash e_1 : \texttt{bool} \rhd \Phi_1$}
      \AXC{$\Phi_1, \Gamma \vdash e_2 : \tau \rhd \Phi_2$}
      \AXC{$\Phi_2, \Gamma \vdash e_2 : \tau \rhd \Phi_3$}
      \TIC{$\Phi, \Gamma
        \vdash \texttt{if } e_1 \texttt{ then } e_2 \texttt{ else } e_3 : \tau
        \rhd \Phi_3$}
    \end{topprooftree}

    \begin{topprooftree}{\textsc{TLetIn}}
      \AXC{$\alpha$ fresh}
      \AXC{$\Phi, \Gamma \vdash e_1 : \alpha \rhd \Phi'$}
      \AXC{$\Phi', \Gamma \oplus x : gen(\Phi', \Gamma, \alpha)
        \vdash e_2 : \tau
        \rhd \Phi''$}
      \TIC{$\Phi, \Gamma
        \vdash \texttt{let } x \texttt{ = } e_1 \texttt{ in } e_2 : \tau
        \rhd \Phi''$}
    \end{topprooftree}

    \begin{topprooftree}{\textsc{TCase}}
      \AXC{$\alpha$ fresh}
      \AXC{$\Phi, \Gamma \vdash e_1 : \alpha? \rhd \Phi_1$}
      \AXC{$\Phi_1, \Gamma \vdash e_2 : \tau \rhd \Phi_2$}
      \AXC{$\Phi_2, \Gamma \oplus x : \alpha
        \vdash e_3 : \tau
        \rhd \Phi_3$}
      \QIC{$\Phi, \Gamma
        \vdash \texttt{case } e_1 \texttt{ of NULL -> } e_2 \texttt{ | } x
        \texttt{ -> } e_3 : \tau
        \rhd \Phi_3$}
    \end{topprooftree}

  \end{mdframed}

  \caption[Règles de typage]{Règles de sous-typage, définissent les jugements
    $\Phi, \Gamma \vdash e : \tau \rhd \Phi'$}
  \label{fig:rulesty}
\end{figure}

La première est la règle de sous-typage des constantes du langage.
Une fonction $T$ existe, qui à une expression $c$ donnée correspondant à une
constante, va associer un type
$t_b(c)$ tel que $t_b(c) \in \{\texttt{int}, \texttt{bool}, \dotsb\}$.

Par exemple, typer l'expression \texttt{3}, qui est une constante entière, se
fera par cette règle, de la façon suivante:
\AX{$\Phi_1 \fCenter \texttt{int} \leqslant \tau \rhd \Phi_2$}
\LL{\textsc{TConst}}
\UIC{$ \Phi_1,\Gamma_1 \fCenter \texttt{3} : \tau \rhd \Phi_2$}
\DP

Un typage complet de l'expression \texttt{3} est disponible en figure
\ref{fig:prf3}.

\begin{rmk}
  Dans \cite{vaugon:tel-01356695}, $T$ est définie pour chaque type de base par
  $T(c) = \forall \alpha . \alpha \mid t_b(c) \leqslant \alpha$.
  On prouve en annexe \ref{sec:prftconst} que ces fonctions sont équivalentes à
  une contrainte transitive près.
\end{rmk}

La règle \textsc{TNull} est strictement la même que \textsc{TConst}, où $c$ est
\texttt{NULL}, et explicite $$T(\texttt{NULL}) = \forall \alpha . \alpha?$$

Un autre exemple de règle de sous-typage est \textsc{TInst}
qui va instancier un schéma de type associé à un identifiant.
Elle va renommer toutes les variables liées au schéma de type,
à la fois dans les contraintes du type et dans le type lui-même,
avec de nouveaux noms, puis chercher à prouver que le nouveau type instancié
est bien sous-type du type $\tau$ final.

\subsection{Règles de relations}

Les jugements de la forme $\Phi \vdash \tau_1 \leqslant \tau_2 \rhd \Phi'$
générés par les règles de la figure \ref{fig:rulesty}
sont prouvables en utilisant les règles de la figure \ref{fig:rulesrel}.

\begin{figure}
  \centering

  \begin{mdframed}
    \begin{subfigure}{1.0\linewidth}

      \begin{topprooftree}{\textsc{LeqNew}}
        \AXC{lorsque $\tau_1 \leqslant \tau_2 \notin \Phi$}
        \AXC{$\Phi, \tau_1 \leqslant \tau_2 \vdash \tau_1 \leq \tau_2
          \rhd \Phi'$}
        \BIC{$\Phi
          \vdash \tau_1 \leqslant \tau_2
          \rhd \Phi'$}
      \end{topprooftree}

      \begin{subfigure}[t]{.5\linewidth}
        \begin{topprooftree}{\textsc{LeqAlreadyProved}}
          \AXC{}
          \UIC{$\Phi, \tau_1 \leqslant \tau_2
            \vdash \tau_1 \leqslant \tau_2
            \rhd \Phi, \tau_1 \leqslant \tau_2$}
        \end{topprooftree}
      \end{subfigure}
      ~
      \begin{subfigure}[t]{.5\linewidth}
        \begin{topprooftree}{\textsc{EQ}}
          \AXC{$\Phi \vdash \tau_1 \leqslant \tau_2 \rhd \Phi'$}
          \AXC{$\Phi'
            \vdash \tau_2 \leqslant \tau_2 \rhd \Phi''$}
          \BIC{$\Phi \vdash
            \tau_1 = \tau_2 \rhd \Phi''$}
        \end{topprooftree}
      \end{subfigure}
      \caption{Règles de comparaison, définissent $\tau_1 \leqslant \tau_2$}
      \label{fig:rulesrelcmp}
    \end{subfigure}

    \begin{subfigure}{1.0\linewidth}
      \begin{topprooftree}{\textsc{CptNew}}
        \AXC{lorsque $\tau_1 \approx \tau_2 \notin \Phi$}
        \AXC{$\Phi, \tau_1 \approx \tau_2 \vdash \tau_1 \simeq \tau_2
          \rhd \Phi'$}
        \BIC{$\Phi
          \vdash \tau_1 \approx \tau_2
          \rhd \Phi'$}
      \end{topprooftree}

      \begin{topprooftree}{\textsc{CptAlreadyProved}}
        \AXC{}
        \UIC{$\Phi, \tau_1 \approx \tau_2
          \vdash \tau_1 \approx \tau_2
          \rhd \Phi, \tau_1 \approx \tau_2$}
      \end{topprooftree}

      \caption{Règles de compatibilité, définissent $\tau_1 \approx \tau_2$}
      \label{fig:rulesrelcpt}
    \end{subfigure}
  \end{mdframed}
  \caption{Règles de relations}
  \label{fig:rulesrel}
\end{figure}

Lorsqu'un but contient une relation qui est déjà dans $\Phi$,
la règle \textsc{AlreadyProved} s'applique et on n'a pas besoin de prouver la
relation à nouveau.

Lorsque le but contient une relation $R$ qui n'est pas déjà dans $\Phi$,
on s'assure de pouvoir l'ajouter sans être incohérent.
La façon dont on opère est qu'on va supposer $R$ vraie, l'ajouter à $\Phi$,
puis prouver sa version alternée. Dans la figure \ref{fig:rulesrel}, on voit
clairement la distinction entre $\leqslant$ et $\leq$ et entre $\approx$ et
$\simeq$. D'une certaine façon, on peut dire que les symboles $\leqslant$ et
$approx$ correspondent à des relations de contrainte qu'on cherche à ajouter à
$\Phi$, et $\leq$ et $\simeq$ des relations entre types que l'on doit prouver
absolument.

Prouver $\tau_1 \leq \tau_2$ ou $\tau_1 \simeq \tau_2$ peut être très facile ou
très complexe. Certains axiomes existent, définis en figure \ref{fig:axioms}.

\begin{figure}
  \centering
  \begin{mdframed}
    \begin{subfigure}[t]{0.5\textwidth}
      \centering
      \begin{topprooftree}{\textsc{LeqSameTy}}
        \AXC{}
        \UIC{$\Phi \vdash \tau \leq \tau \rhd \Phi$}
      \end{topprooftree}
      \begin{topprooftree}{\textsc{LeqBaseNull}}
        \AXC{}
        \UIC{$\Phi \vdash t_b \leq t_b? \rhd \Phi$}
      \end{topprooftree}
      \caption{Axiomes de comparaisons}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.5\textwidth}
      \centering
      \begin{topprooftree}{\textsc{CptSameTy}}
        \AXC{}
        \UIC{$\Phi \vdash \tau \simeq \tau \rhd \Phi$}
      \end{topprooftree}
      \begin{topprooftree}{\textsc{CptBaseNull}}
        \AXC{}
        \UIC{$\Phi \vdash t_b \simeq t_b? \rhd \Phi$}
      \end{topprooftree}
      \caption{Axiomes de compatibilités}
    \end{subfigure}
  \end{mdframed}
  \caption{Axiomes de relations}
  \label{fig:axioms}
\end{figure}

Pour le reste des cas, il y'a deux possibilités: soit on a deux types de base
$t_{b1}$ et $t_{b2}$ qui ne satisfont pas les axiomes, auquel cas on a un clash de
type, soit on a une variable et un autre type (potentiellement une autre
variable) en relation, et il faut s'assurer que cette relation n'entre pas en
contradiction avec les contraintes déjà dans $\Phi$.
Cette étape de vérification de toutes les contraintes de $\Phi$ liées à une
variable et un type auquel elle est en relation s'appelle la \emph{saturation}
de $\Phi$.

\begin{definition}[Saturation de $\Phi$]
  $\Phi$ est saturé pour une $R$ entre une variable $\alpha$ et un type $\tau$ si,
  pour toute contrainte entre $\alpha$ et un type $\tau'$ dans $\Phi$,
  on a aussi une contrainte \og satisfaisante\fg{} dans $\Phi$ entre $\tau$ et
  $\tau'$.

  Les contraintes \og satisfaisantes\fg{} sont telles que:
  \begin{itemize}
  \item Si $R = \alpha \leq \tau$ alors
    \begin{itemize}
    \item $\forall \tau' \mid \alpha \leqslant \tau' \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \item $\forall \tau' \mid \tau' \leqslant \alpha \in \Phi
      \implies \tau' \leqslant \tau \in \Phi$
    \item $\forall \tau' \mid \alpha \approx \tau' \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \end{itemize}
  \item Si $R = \tau \leq \alpha$ alors
    \begin{itemize}
    \item $\forall \tau' \mid \alpha \leqslant \tau' \in \Phi
      \implies \tau \leqslant \tau' \in \Phi$
    \item $\forall \tau' \mid \tau' \leqslant \alpha \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \item $\forall \tau' \mid \alpha \approx \tau' \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \end{itemize}
  \item Si $R = \alpha \simeq \tau$ alors
    \begin{itemize}
    \item $\forall \tau' \mid \alpha \leqslant \tau' \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \item $\forall \tau' \mid \tau' \leqslant \alpha \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \item $\forall \tau' \mid \alpha \approx \tau' \in \Phi
      \implies \tau \approx \tau' \in \Phi$
    \end{itemize}
  \end{itemize}
\end{definition}

La saturation de $\Phi$ par rapport à une relation est vérifiée par les règles
de la figure \ref{fig:rulessat}.

\begin{figure}
  \centering
  \begin{topprooftree}{\textsc{LeqVarEnd}}
    \AXC{lorsque $\alpha \neq \tau'$}
    \noLine
    \UIC{lorsque $\forall \tau \mid \alpha \leqslant \tau \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \AXC{lorsque $\forall \tau \mid \tau \leqslant \alpha \in \Phi
      \implies \tau \leqslant \tau' \in \Phi$}
    \noLine
    \UIC{lorsque $\forall \tau \mid \alpha \approx \tau \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \BIC{$\Phi \vdash \alpha \leq \tau' \rhd \Phi$}
  \end{topprooftree}
  \begin{topprooftree}{\textsc{GeqVarEnd}}
    \AXC{lorsque $\alpha \neq \tau'$}
    \noLine
    \UIC{lorsque $\forall \tau \mid \alpha \leqslant \tau \in \Phi
      \implies \tau' \leqslant \tau \in \Phi$}
    \AXC{lorsque $\forall \tau \mid \tau \leqslant \alpha \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \noLine
    \UIC{lorsque $\forall \tau \mid \alpha \approx \tau \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \BIC{$\Phi \vdash \tau' \leq \alpha \rhd \Phi$}
  \end{topprooftree}
  \begin{topprooftree}{\textsc{CptVarEnd}}
    \AXC{lorsque $\alpha \neq \tau'$}
    \noLine
    \UIC{lorsque $\forall \tau \mid \alpha \leqslant \tau \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \AXC{lorsque $\forall \tau \mid \tau \leqslant \alpha \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \noLine
    \UIC{lorsque $\forall \tau \mid \alpha \approx \tau \in \Phi
      \implies \tau \approx \tau' \in \Phi$}
    \BIC{$\Phi \vdash \alpha \simeq \tau' \rhd \Phi$}
  \end{topprooftree}
  \caption{Vérifications de saturation}
  \label{fig:rulessat}
\end{figure}

Si $\Phi$ n'est pas saturé pour la relation à prouver, et qu'il n'y a pas
d'erreur, ça veut dire qu'on est en \emph{cours} de saturation, et on va
temporairement rajouter des contraintes dans $\Phi$ jusqu'à saturation.
Les règles correspondant à ces cas sont présentées en figure
\ref{fig:rulestrans}.

\begin{figure}
  \centering
  \begin{mdframed}
    \newcommand{\transrule}[5]{\begin{topprooftree}{\textsc{#1}}
        \AXC{lorsque $\alpha \neq \tau'$ et $#3 \notin \Phi$}
        \AXC{$\Phi, #2, #3 \vdash #4 \rhd \Phi'$}
        \AXC{$\Phi' \vdash #5 \rhd \Phi''$}
        \TIC{$\Phi, #2 \vdash #4 \rhd \Phi''$}
      \end{topprooftree}}
    \begin{subfigure}{1.0\linewidth}
      \transrule{LeqVarLeqTy}{\alpha \leqslant \tau}{\tau \approx \tau'}
      {\alpha \leq \tau'}{\tau \simeq \tau'}

      \transrule{LeqTyLeqVar}{\tau \leqslant \alpha}{\tau \leqslant \tau'}
      {\alpha \leq \tau'}{\tau \leq \tau'}

      \transrule{LeqVarCptTy}{\alpha \approx \tau}{\tau \approx \tau'}
      {\alpha \leq \tau'}{\tau \simeq \tau'}

      \transrule{GeqVarLeqTy}{\alpha \leqslant \tau}{\tau' \leqslant \tau}
      {\tau' \leq \alpha}{\tau' \leq \tau}
      
      \transrule{GeqTyLeqVar}{\tau \leqslant \alpha}{\tau \approx \tau'}
      {\tau' \leq \alpha}{\tau' \simeq \tau}

      \transrule{GeqVarCptTy}{\alpha \approx \tau}{\tau \approx \tau'}
      {\tau' \leq \alpha}{\tau' \simeq \tau}
      
      \caption{Règles de saturation comparaison, définissent $\tau_1 \leq \tau_2$}
      \label{fig:rulestranscmp}
    \end{subfigure}

    \begin{subfigure}{1.0\linewidth}

      \transrule{CptVarLeqTy}{\alpha \leqslant \tau}{\tau \approx \tau'}
      {\alpha \simeq \tau'}{\tau \simeq \tau'}
      
      \transrule{CptTyLeqVar}{\tau \leqslant \alpha}{\tau \approx \tau'}
      {\alpha \simeq \tau'}{\tau \simeq \tau'}

      \transrule{CptVarCptTy}{\alpha \approx \tau}{\tau \approx \tau'}
      {\alpha \simeq \tau'}{\tau \simeq \tau'}

      \caption{Règles de saturation de compatibilité, définissent $\tau_1 \simeq \tau_2$}
      \label{fig:rulestranscpt}
    \end{subfigure}
  \end{mdframed} 
  \caption{Règles de saturation}
  \label{fig:rulestrans}
\end{figure}

\subsection{Généralisation}

Après avoir contraint le type $\tau$ d'une expression avec les règles définies,
au moment de l'ajouter dans l'environnement $\Gamma$,
après un \texttt{let} par exemple,
il faut \og généraliser\fg{} le type $\tau$,
c'est à dire créer un schéma de type $\sigma$ qui va lier les variables
mentionnées dans $\tau$, et les contraindre dans un $\Phi\!\mid_\alpha$.

La fonction $gen(\Phi,\Gamma,\tau)$ est définie en figure \ref{fig:gen}.

\begin{figure}
  \centering
  \begin{mdframed}
    \begin{align*}
      gen(\Phi,\Gamma,\tau) = ...
    \end{align*}
  \end{mdframed}
  \caption{Fonctions de généralisation}
  \label{fig:gen}
\end{figure}

Par exemple,
$$gen(\{\alpha_{n+1} \approx \alpha_{n+2},
\alpha_n \leqslant \texttt{int}\},\emptyset, \alpha_n) = \forall
\alpha . \alpha \mid \alpha \leqslant \texttt{int}$$

\subsection{Exemples de typage}

En utilisant ces règles, et les nombreuses autres
présentées dans \cite{mauny2014nullable}, on peut inférer le type de toutes
sortes d'expressions, comme par exemple les
expressions \texttt{3} et \texttt{true}, dont les preuves sont respectivement
présentées en figures \ref{fig:prf3} et \ref{fig:prftrue}.

Dans tous les cas, le typage d'une expression se retrouvera être un
schéma de type contraint sur certaines variables, plutôt qu'un type concret
$\tau$. Les contraintes des types seront accumulées, et à chaque fois qu'une
nouvelle contrainte est ajoutée dans le contexte, on vérifie sa compatibilité
avec les autres.

Si une contrainte sensée être ajoutée par une règle est incompatible avec une
contrainte déjà dans $\Phi$, ou si aucune règle ne peut s'applique,
il y a \textit{clash} et on ne peut pas typer le
programme ou l'expression.

Un exemple de clash est présenté en figure \ref{fig:clash}.

\begin{figure}
  \centering
  \begin{prooftree} 
    \AXC{}
    \LL{\textsc{ERREUR}}
    \UIC{$\Phi_1, \texttt{bool}
      \leqslant \texttt{int} \nvdash \texttt{bool} \leq \texttt{int} \rhd \Phi_2$}
    \LL{\textsc{LeqNew}}
    \UIC{$ \Phi_1 \fCenter \texttt{bool} \leqslant \texttt{int} \rhd \Phi_2$}
    \LL{\textsc{TConst}}
    \UIC{$ \Phi_1,\Gamma \fCenter \texttt{true} : \texttt{int} \rhd \Phi_2$   }
  \end{prooftree}

  Il est bien sûr possible de définir que \texttt{bool} $\leq$ \texttt{int},\\
  mais cet exemple minimal illustre comment une erreur de typage se produit.
  
  \caption{Clash entre l'expression \texttt{true} et le type \texttt{int}}
  \label{fig:clash} 
\end{figure}


\begin{sidewaysfigure}
  \begin{prooftree}
    \AXC{}
    \LL{\textsc{GeqVarEnd}}
    \UIC{$\Phi_1, \texttt{int} \leqslant \alpha,
      \texttt{int} \approx \texttt{int} \vdash \texttt{int} \leq \alpha \rhd
      \Phi_2$}
    \AXC{}
    \LL{\textsc{CptBaseTy}}
    \UIC{$\Phi_2 \vdash \texttt{int} \simeq \texttt{int} \rhd \Phi_2$}
    \LL{\textsc{GeqTyLeqVar}}
    \BIC{$\Phi_1, \texttt{int}
      \leqslant \alpha \vdash \texttt{int} \leq \alpha \rhd \Phi_2$}
    \LL{\textsc{LeqNew}}
    \UIC{$ \Phi_1 \fCenter \texttt{int} \leqslant \alpha \rhd \Phi_2$}
    \LL{\textsc{TConst}}
    \UIC{$ \Phi_1,\Gamma \fCenter \texttt{3} : \alpha \rhd \Phi_2$}
  \end{prooftree}

  \centering
   $\Phi_2 = \{\texttt{int} \leqslant \alpha, \texttt{int} \approx \texttt{int}\}$

   En enlevant les axiomes, $\Phi_2 = \{\texttt{int} \leqslant \alpha\}$
  \caption{Typage de l'expression \texttt{3}}
  \label{fig:prf3}
  
  \begin{prooftree}
    \AXC{}
    \LL{\textsc{GeqVarEnd}}
    \UIC{$\Phi_1, \texttt{bool} \leqslant \alpha,
      \texttt{bool} \approx \texttt{bool} \vdash \texttt{bool} \leq \alpha \rhd
      \Phi_2$}
    \AXC{}
    \LL{\textsc{CptBaseTy}}
    \UIC{$\Phi_2 \vdash \texttt{bool} \simeq \texttt{bool} \rhd \Phi_2$}
    \LL{\textsc{GeqTyLeqVar}}
    \BIC{$\Phi_1, \texttt{bool}
      \leqslant \alpha \vdash \texttt{bool} \leq \alpha \rhd \Phi_2$}
    \LL{\textsc{LeqNew}}
    \UIC{$ \Phi_1 \fCenter \texttt{bool} \leqslant \alpha \rhd \Phi_2$}
    \LL{\textsc{TConst}}
    \UIC{$ \Phi_1,\Gamma \fCenter \texttt{true} : \alpha \rhd \Phi_2$}
  \end{prooftree}
  \caption{Typage de l'expression \texttt{true}}
  \label{fig:prftrue}
\end{sidewaysfigure}

\section{Implémentation en OCaml}

\section{Conclusion}

(todo later)
\begin{appendices}
 \section{Preuve de l'équivalence des fonctions $T$}
 \label{sec:prftconst}

 Les figures \ref{fig:prf3} et \ref{fig:prftrue} présentent des preuves de
 typages utilisant la règle \textsc{TConst} présentée en figure \ref{fig:rulesty}
 où la fonction $T$ renvoie directement un type concret.

 En sortie de typage de chacune de ces constante, l'ensemble des contraintes
 $\Phi_A$ est tel que
 $$\Phi_A = \{t_b(c) \leqslant \alpha\}$$
 La figure \ref{fig:prf3thesis} type une constante en utilisant la fonction $T$
 originale qui renvoie un schéma de type. En sortie de typage, on a
 $$\Phi_B = \{t_b(c) \leqslant \alpha_1, \alpha_1 \leqslant \alpha\}$$
 Or, la relation $\leqslant$ est transitive (preuve dans
 \cite{vaugon:tel-01356695}), donc
 $$\Phi_B \vdash \Phi_A$$

 On peut définir des règles d'exploitation de cette transitivité, et on pourra
 toujours prouver $t_b(c) \leqslant \alpha$ à partir de $\Phi_B$.

 Dans l'autre sens, la preuve en figure \ref{fig:prf3thesis} crée $\alpha_1$
 localement à cause de l'instantiation. Parce que c'est une instance de variable
 liée au schéma de type instancié, aucune autre instance de cette variable peut
 se trouver ailleurs que dans cette preuve de \textsc{TConst}.
 Les contraintes y faisant référence dans $\Phi$ ne seront utiles que pour
 prouver $t_b(c) \leqslant \alpha$ par transitivité.

 Ainsi, remplacer $\Phi_B$ par $\Phi_A$ dans un $\Phi_C$ plus grand n'aura aucun
 effet secondaire sur le typage de futures expressions.
 
 \begin{sidewaysfigure}
   \begin{prooftree}
     \AXC{}
     \LL{\textsc{LeqVarEnd}}
     \UIC{$\Phi_0, \alpha \approx \alpha \vdash \alpha_1 \leq \alpha \rhd
       \Phi_1$}
     \AXC{}
     \LL{\textsc{CptSameVar}}
     \UIC{$\Phi_1 \vdash \alpha \simeq \alpha \rhd \Phi_1$}
     \LL{\textsc{LeqVarLeqTy}}
     \BI$\Phi_0 = \{\alpha_1 \leqslant \alpha\} \fCenter
     \alpha_1 \leq \alpha \rhd \Phi_1$
     \LL{\textsc{LeqNew}}
     \UIC{$ \emptyset \fCenter \alpha_1 \leqslant \alpha \rhd \Phi_1$}
     \AXC{}
     \LL{\textsc{GeqVarEnd}}
     \UIC{$\Phi_2, \texttt{int} \approx \texttt{int} \vdash t_b(c) \leq \alpha_1 \rhd
       \Phi_3$}
     \AXC{}
     \LL{\textsc{CptBaseTy}}
     \UIC{$\Phi_3 \vdash \texttt{int} \simeq \texttt{int} \rhd \Phi_3$}
     \LL{\textsc{GeqVarLeqTy}}
     \BI$\Phi_2 = \{\Phi_1,t_b(c) \leqslant \alpha_1\} \fCenter t_b(c) \leq
     \alpha_1 \rhd \Phi_3$
     \LL{\textsc{LeqNew}}
     \UIC{$ \Phi_1 \fCenter t_b(c) \leqslant \alpha_1 \rhd \Phi_3$}
     \LL{\textsc{Inst}}
     \BI$\emptyset \fCenter [\forall \alpha_0 . \alpha_0 \mid t_b(c)
     \leqslant \alpha_0] \leqslant \alpha \rhd \Phi_3$
     \LL{\textsc{TConst}}
     \UIC{$ \emptyset, \emptyset \fCenter c : \alpha \rhd \Phi_3$}
   \end{prooftree}
   \centering
   $\Phi_3 = \{\alpha_1 \leqslant \alpha, \alpha \approx \alpha,
     t_b(c) \leqslant \alpha_1, \alpha_1 \approx \alpha_1\}$

   En enlevant les axiomes, $\Phi_3 = \{\alpha_1 \leqslant \alpha, t_b(c)
     \leqslant \alpha_1\}$

   \caption{Typage de l'expression \texttt{3}
     avec $T(3) =[\forall \alpha_0 . \alpha_0 \mid
     \texttt{int} \leqslant \alpha_0] $}
   \label{fig:prf3thesis}
\end{sidewaysfigure} 

\end{appendices}
