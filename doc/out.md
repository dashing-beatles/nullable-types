---
institute: Sorbonne Université
title: Inférence de types nullable
subtitle: Rapport de projet
author:
- Clément Busschaert
- Alexandre Doussot
course: TAS
place: Paris
date: \today
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\newpage

\setmonofont{DejaVu Sans}

\makeatletter

\patchcmd{\lsthk@SelectCharTable}{%
  \lst@ifbreaklines\lst@Def{`)}{\lst@breakProcessOther)}\fi
}{%
}{
}{
}

\makeatother

\newcommand{\CodeSymbol}[1]{\textcolor{cyan!50!black}{#1}}
\newcommand{\CodeSymbolPurple}[1]{\textcolor{purple}{#1}}

\lstset{
basicstyle=\footnotesize\ttfamily\bfseries\color{red},
keywordstyle=\color{green!50!black},
keywordstyle=[2]\color{red!50!black},
keywordstyle=[3]\color{purple},
keywordstyle=[4]\color{red},
stringstyle=\color{yellow!80!black},
commentstyle=\itshape\color{gray!50!white},
identifierstyle=\color{cyan!50!black},
literate={\{}{{\CodeSymbol{\{}}}1
               {\}}{{\CodeSymbol{\}}}}1
               {(}{{\CodeSymbol{(}}}1
               {)}{{\CodeSymbol{)}}}1
               {=}{{\CodeSymbol{=}}}1
               {;}{{\CodeSymbol{;}}}1
               {.}{{\CodeSymbol{.}}}1
               {:}{{\CodeSymbol{:}}}1
               {|}{{\CodeSymbol{|}}}1
               {,}{{\CodeSymbol{,}}}1
               {-}{{\CodeSymbol{-}}}1
               {1}{{\CodeSymbol{1}}}1
               {2}{{\CodeSymbol{2}}}1
               {3}{{\CodeSymbol{3}}}1
               {4}{{\CodeSymbol{4}}}1
               {5}{{\CodeSymbol{5}}}1
               {6}{{\CodeSymbol{6}}}1
               {7}{{\CodeSymbol{7}}}1
               {8}{{\CodeSymbol{8}}}1
               {9}{{\CodeSymbol{9}}}1
               {0}{{\CodeSymbol{0}}}1
               {->}{{\CodeSymbol{->}}}1
               {<-}{{\CodeSymbol{<-}}}1
               {+}{{\CodeSymbol{+}}}1
               {-}{{\CodeSymbol{-}}}1
               {*}{{\CodeSymbol{*}}}1
}

\lstdefinelanguage{SOL}{
keywords={},
morestring=[b]{"},
morestring=[b]{'},
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morecomment=[s][\color{black}]{<}{>}
}

\lstdefinelanguage{JavaScript} {
morekeywords={break,case,catch,continue,debugger,default,delete,do,else,finally,
  for,function,if,in,instanceof,new,return,switch,this,throw,try,typeof,var,void,while, True, False,
  node, with, machine, memory,instances, prototype, list, let, print, type, of},
morekeywords=[2]{interface, class,enum,export,extends,import,super,implements,package,private,protected,public,static,yield},
morekeywords=[3]{True, False, Int, Binop, Not, Or, And, String, Var, Print, Let, If},
morekeywords=[4]{}
morestring=[b]{"},
morestring=[b]{'},
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morecomment=[s][\color{black}]{<}{>}
}


\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\lstset{escapeinside={(*@}{@*)}}

\newpage

# Réalisation

## Langage

Le langage source est un de la famille des ML.
Il en possède les principales caractéristiques, à savoir
un mécanisme d'abstraction/applications ainsi que des fonctions
de première classe.

### Syntaxe
 
\begin{figure}
\begin{grammar}

<terminal> ::= $()$
\alt $int$
\alt $true$
\alt $false$
\alt $id$

<exp> ::= <exp> <op> <exp>
\alt \verb|if| <exp> verb|then| <exp> verb|else| <exp>
\alt \verb|not| <exp>
\alt \verb|function| $id$ \verb|->| <exp>
\alt <exp> <exp>
\alt <terminal>

<instr> ::= <exp>
\alt \verb|let| $id$ = <exp>

<op> ::= \verb|+|
\alt \verb|-|
\alt \verb|*|
\alt \verb|AND|
\alt \verb|OR|

\end{grammar}
\hrulefill
\caption{Language grammar}
\label{grammar}
\end{figure}

### Parsing

La parsing utilise `ocamllex` et `ocamllex`, et est réalisé utilisant
les fichiers données dans le typeur de lambda calcul.

Le codé est parsé en l'AST suivant.

```ocaml
type ml_expr =
  | Const of ml_const
  | Var of string
  | IfThenElse of ml_expr * ml_expr * ml_expr
  | Case of ml_expr * ml_expr * ml_expr
  | App of ml_expr * ml_expr
  | Abs of string * ml_expr
  | Let of string * ml_expr

and ml_const =
  | Int of int
  | Null
  | Unit
  | Bool of bool
```

## Preuve

## Environnement

### Phi
  $\Phi$ est un set de contraintes, implémenté sous la forme d'une liste.
```ocaml
  type(phi) = constr list
```
Updates to $\Phi$ are implemented using multiple functions.
```ocaml
let get_phi, update_local_phi, update_persistent_phi, in_phi, reset_phi =
  let local_phi = ref [] in
  let global_phi = ref [] in
  (fun () -> !local_phi @ !global_phi),
  (fun (constr:constr) -> if not (List.mem constr !local_phi) then local_phi := constr::!local_phi),
  (fun constr -> if not (List.mem constr !global_phi) then global_phi := constr::!global_phi),
  (fun constr -> List.mem constr (!local_phi @ !global_phi)),
  (fun () -> local_phi := [])
```



Comme dit dans le papier, seules `TInst`, `LEQNew` et `CPTNew` modifient réellement
$\Phi$. Ceci est réprésenté par la dichotomie entre un $\Phi$ local et un $\Phi$ 
persistent. Les règles susmentionnées font appel à `update_persistent_phi`.
`local_phi` est effacé à chaque nouveau typage.

### Gamma

$\Gamma$ est une map, d'identifiants vers des schémas de type. Elle est implémentée
sous la forme d'une liste associative.
Son fonctionnement est similaire à $\Phi$.

### Schémas de types

Rappelons qu'un schéma de types est de la forme `FIXME beau latex ici`. \\
Naturellement, cela se représente sous la forme d'un record.

```ocaml
type quantified_type =
  Forall of (atom list) * atom

and sigma = {quant: quantified_type; constrs: constr list}
```


## Règles

### Organisation

La création/résolution de contraintes est faite de manière recursive
sur les différentes règles. La fonction principale
`type_rec` pattern-matche sur ce qu'on essaie de prouver.

```ocaml
let type_rec goal =
  if !Options.print_step then
    print_step goal (get_phi ()) (get_rule()) |> print_endline;
  match goal with
  | Expr(expr, atom) -> type_exp expr atom
  | Constr(constr) -> type_constr constr
  | Finished -> ()
```

### Règles de sous-typage

Les règles de sous-typage sont dirigées par la syntaxe.
À chaque expression sa règle, qui génère un ou plusieurs sous buts.

Le processus est similaire pour toutes les règles, nous n'en détaillerons donc
qu'une ici.

\begin{topprooftree}{\textsc{TApp}}
  \AXC{$\alpha$ fresh}
  \AXC{$\Phi, \Gamma \vdash e_1 : \alpha \to \tau \rhd \Phi'$}
  \AXC{$\Phi', \Gamma \vdash e_2 : \alpha \rhd \Phi''$}
  \TIC{$\Phi, \Gamma
    \vdash e_1 e_2 : \tau
    \rhd \Phi''$}
\end{topprooftree}

```ocaml
let type_app f x atom =
  let alpha = fresh () in
  set_rule "TApp (1)";
  (* First subgoal recursion *)
  type_rec @@ Expr(f, alpha => atom));
  set_rule "TApp (2)";
  (* Second subgoal recursion *)
  type_rec @@ Expr(x, alpha)
```

`set_rule` permet à l'afficheur d'afficher la règle dans laquelle on se trouve.

`type_rec` est l'appel à la fonction récursive permettant de continuer le typage.

### Règles terminales

Quant une règle est terminale - c'est à dire qu'elle n'a pas de sous-buts
en hypothèse, elle rappelle type_rec avec une valeur de fin, `Finished`.

```ocaml
  | _, Alpha a when geq_var_end ~tau:left ~alpha:a ->
        set_rule "GeqVarEnd";
        type_rec Finished
```

Ceci permet d'effectuer un dernier tour de boucle et d'afficher la règle finale.


### Règles de saturation

Comme vu précédemment, il existe un grand nombre de règles similaires, qu'il
est difficile d'implémenter sans erreur humaine.
Cependant, nous avons pu voir que certaines caractéristiques pouvaient être abstraites.


![Abstraction elements](ineq_rule_elt.png){width=70%}

Nous avons donc décidé d'utiliser une fonction d'usine: `make_rule`. `make_rule`
takes a record describing the rule:

```ocaml
let leq_var_leq_ty_record = {
    in_phi = get_phi_alpha_leq_tau;
    not_in_phi = equivalent_in_phi;
    add_phi = make_compat;
    first_goal = make_flatineq_goal;
    sec_goal = make_flatcompat_goal;
    name = "LeqVarLeqTy"
  }
```

and produces a rule function in the following fashion.

```ocaml
let rec make_rule elts =
  (fun alpha tau_prime ->
    (* Find constraints where alpha is in *)
    let taus = elts.in_phi alpha in 
    (* Remove those that we don't want to have *)
    let taus = taus |> List.filter (fun tau -> not(elts.not_in_phi tau_prime tau)) in
      (* Add to local constraint environment *)
      taus |> List.iter (function tau -> update_local_phi @@ elts.add_phi tau tau_prime);
      (* Then recurse, similarly at what we showed for TApp *)
      taus |> List.iter (function tau ->
        set_rule @@ elts.name ^ " (1)";
        type_rec @@ elts.first_goal alpha tau_prime;
        set_rule @@ elts.name ^ "(2)";
        type_rec @@ elts.sec_goal tau tau_prime
        ))
```

## Repl

### mode print-step

La repl permet d'afficher chaque étape de la preuve de typage.

```ocaml
$ [] ⊢ 3 : 'a (start)
[] ⊢ int ≼ 'a (TConst)
[int ≼ 'a] ⊢ int ≤ 'a (LEQNew)
[int ≈ int ∧ int ≼ 'a] ⊢ int ≤ 'a (GeqTyLeqVar (1))
[int ≈ int ∧ int ≼ 'a] ⊢ ⊤ (GeqVarEnd)
[int ≈ int ∧ int ≼ 'a] ⊢ int ≃ int (GeqTyLeqVar(2))
[int ≈ int ∧ int ≼ 'a] ⊢ ⊤ (CptBaseTy)
 - int
```

## Usage

La repl s'invoque simplement en lançant l'exécutable.
Il suffit alors d'entrer ce que l'on veut typer.
Cependant, il est possible de changer le comportement de l'afficheur.
Plusieurs flags sont disponibles.

* `--print-step`: La repl affichera chaque opération de contrainte ainsi que l'environnement associé
* `--print-phi`: La repl affichera la liste de contraintes en sortie de preuve de typage.
* `--print-sigma`: La repla affichera le schéma de type rendu par la généralisation.
