---
institute: Sorbonne Université
title: Inférence de types nullable
subtitle: Rapport de projet
author:
- Clément Busschaert
- Alexandre Doussot
course: TAS
place: Paris
date: \today
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\newpage

\include tex_templates/code.tex

\newpage

# Réalisation

\include language.md

## Preuve

## Environnement

### Phi
  $\Phi$ est un set de contraintes, implémenté sous la forme d'une liste.
```ocaml
  type(phi) = constr list
```
Updates to $\Phi$ are implemented using multiple functions.
```ocaml
let get_phi, update_local_phi, update_persistent_phi, in_phi, reset_phi =
  let local_phi = ref [] in
  let global_phi = ref [] in
  (fun () -> !local_phi @ !global_phi),
  (fun (constr:constr) -> if not (List.mem constr !local_phi) then local_phi := constr::!local_phi),
  (fun constr -> if not (List.mem constr !global_phi) then global_phi := constr::!global_phi),
  (fun constr -> List.mem constr (!local_phi @ !global_phi)),
  (fun () -> local_phi := [])
```



Comme dit dans le papier, seules `TInst`, `LEQNew` et `CPTNew` modifient réellement
$\Phi$. Ceci est réprésenté par la dichotomie entre un $\Phi$ local et un $\Phi$ 
persistent. Les règles susmentionnées font appel à `update_persistent_phi`.
`local_phi` est effacé à chaque nouveau typage.

### Gamma

$\Gamma$ est une map, d'identifiants vers des schémas de type. Elle est implémentée
sous la forme d'une liste associative.
Son fonctionnement est similaire à $\Phi$.

### Schémas de types

Rappelons qu'un schéma de types est de la forme `FIXME beau latex ici`. \\
Naturellement, cela se représente sous la forme d'un record.

```ocaml
type quantified_type =
  Forall of (atom list) * atom

and sigma = {quant: quantified_type; constrs: constr list}
```


## Règles

### Organisation

La création/résolution de contraintes est faite de manière recursive
sur les différentes règles. La fonction principale
`type_rec` pattern-matche sur ce qu'on essaie de prouver.

```ocaml
let type_rec goal =
  if !Options.print_step then
    print_step goal (get_phi ()) (get_rule()) |> print_endline;
  match goal with
  | Expr(expr, atom) -> type_exp expr atom
  | Constr(constr) -> type_constr constr
  | Finished -> ()
```

### Règles de sous-typage

Les règles de sous-typage sont dirigées par la syntaxe.
À chaque expression sa règle, qui génère un ou plusieurs sous buts.

Le processus est similaire pour toutes les règles, nous n'en détaillerons donc
qu'une ici.

\include tapp.tex

```ocaml
let type_app f x atom =
  let alpha = fresh () in
  set_rule "TApp (1)";
  (* First subgoal recursion *)
  type_rec @@ Expr(f, alpha => atom));
  set_rule "TApp (2)";
  (* Second subgoal recursion *)
  type_rec @@ Expr(x, alpha)
```

`set_rule` permet à l'afficheur d'afficher la règle dans laquelle on se trouve.

`type_rec` est l'appel à la fonction récursive permettant de continuer le typage.

### Règles terminales

Quant une règle est terminale - c'est à dire qu'elle n'a pas de sous-buts
en hypothèse, elle rappelle type_rec avec une valeur de fin, `Finished`.

```ocaml
  | _, Alpha a when geq_var_end ~tau:left ~alpha:a ->
        set_rule "GeqVarEnd";
        type_rec Finished
```

Ceci permet d'effectuer un dernier tour de boucle et d'afficher la règle finale.


### Règles de saturation

Comme vu précédemment, il existe un grand nombre de règles similaires, qu'il
est difficile d'implémenter sans erreur humaine.
Cependant, nous avons pu voir que certaines caractéristiques pouvaient être abstraites.


![Abstraction elements](ineq_rule_elt.png){width=70%}

Nous avons donc décidé d'utiliser une fonction d'usine: `make_rule`. `make_rule`
takes a record describing the rule:

```ocaml
let leq_var_leq_ty_record = {
    in_phi = get_phi_alpha_leq_tau;
    not_in_phi = equivalent_in_phi;
    add_phi = make_compat;
    first_goal = make_flatineq_goal;
    sec_goal = make_flatcompat_goal;
    name = "LeqVarLeqTy"
  }
```

and produces a rule function in the following fashion.

```ocaml
let rec make_rule elts =
  (fun alpha tau_prime ->
    (* Find constraints where alpha is in *)
    let taus = elts.in_phi alpha in 
    (* Remove those that we don't want to have *)
    let taus = taus |> List.filter (fun tau -> not(elts.not_in_phi tau_prime tau)) in
      (* Add to local constraint environment *)
      taus |> List.iter (function tau -> update_local_phi @@ elts.add_phi tau tau_prime);
      (* Then recurse, similarly at what we showed for TApp *)
      taus |> List.iter (function tau ->
        set_rule @@ elts.name ^ " (1)";
        type_rec @@ elts.first_goal alpha tau_prime;
        set_rule @@ elts.name ^ "(2)";
        type_rec @@ elts.sec_goal tau tau_prime
        ))
```

## Repl

### mode print-step

La repl permet d'afficher chaque étape de la preuve de typage.

```ocaml
$ [] ⊢ 3 : 'a (start)
[] ⊢ int ≼ 'a (TConst)
[int ≼ 'a] ⊢ int ≤ 'a (LEQNew)
[int ≈ int ∧ int ≼ 'a] ⊢ int ≤ 'a (GeqTyLeqVar (1))
[int ≈ int ∧ int ≼ 'a] ⊢ ⊤ (GeqVarEnd)
[int ≈ int ∧ int ≼ 'a] ⊢ int ≃ int (GeqTyLeqVar(2))
[int ≈ int ∧ int ≼ 'a] ⊢ ⊤ (CptBaseTy)
 - int
```

## Usage

La repl s'invoque simplement en lançant l'exécutable.
Il suffit alors d'entrer ce que l'on veut typer.
Cependant, il est possible de changer le comportement de l'afficheur.
Plusieurs flags sont disponibles.

* `--print-step`: La repl affichera chaque opération de contrainte ainsi que l'environnement associé
* `--print-phi`: La repl affichera la liste de contraintes en sortie de preuve de typage.
* `--print-sigma`: La repla affichera le schéma de type rendu par la généralisation.