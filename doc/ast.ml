type ml_expr =
  | Const of ml_const
  | Var of string
  | IfThenElse of ml_expr * ml_expr * ml_expr
  | Case of ml_expr * ml_expr * ml_expr
  | App of ml_expr * ml_expr
  | Abs of string * ml_expr
  | Let of string * ml_expr

and ml_const =
  | Int of int
  | Null
  | Unit
  | Bool of bool