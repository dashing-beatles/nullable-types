## Langage

Le langage source est un de la famille des ML.
Il en possède les principales caractéristiques, à savoir
un mécanisme d'abstraction/applications ainsi que des fonctions
de première classe.

### Syntaxe
 
\include syntax.tex

### Parsing

La parsing utilise `ocamllex` et `ocamllex`, et est réalisé utilisant
les fichiers données dans le typeur de lambda calcul.

Le codé est parsé en l'AST suivant.

```ocaml
\include ast.ml
```