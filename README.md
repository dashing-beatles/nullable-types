# nullable-types

## Compilation

`make` should build the project, provided `ocamlc`, `ocamlyacc` and `ocamllex` are on the `$PATH`

## Running

Once built, a file called `intertypeur` should be availabe and executable.
