open Constraints
open Printer

let same_var alpha beta
  = match alpha, beta with
  | Typevar a, Typevar b
  | Typevar a, Freshvar b
  | Freshvar a, Typevar b
  | Freshvar a, Freshvar b
    -> a = b
  | _ -> false

let rec add_var l v =
  match l with
  | [] -> [v]
  | h::t -> if same_var h v then l else h::(add_var t v)

let rec merge l1 l2 =
  match l1 with
  | [] -> l2
  | h::t -> merge t (add_var l2 h)

let merge_lists l =
  let rec merge' acc l =
      match l with
      | [] -> acc
      | h::t -> merge' (merge acc h) t
  in merge' [] l

let rec filter_map f l =
  match l with
  | [] -> []
  | h::t -> (match (f h) with
      | None -> filter_map f t
      | Some x -> x::(filter_map f t))

let rec ftv_type tau =
  let mkvar = fun n -> Freshvar n in
  match tau with
  | Type _ -> []
  | Typevar n -> [mkvar n]
  | Freshvar n -> [mkvar n]
  | Arrow (t1, t2) -> merge (ftv_type t1) (ftv_type t2)
  | Nullable t -> ftv_type t
  | Sigma s -> ftv_sigma s
and ftv_sigma {quant=Forall (btv, t); constrs=_} =
  List.filter (fun alpha -> List.for_all
                  (fun x -> same_var alpha x |> not)
                  btv)
  @@ ftv_type t


let rec ftv_env gamma =
  merge_lists @@ List.map (fun (_, s) -> ftv_sigma s) gamma

let dep_tys alpha = filter_map
    (function
      | Ineq(a,b) when same_var a alpha -> Some b
      | Ineq(a,b) when same_var b alpha -> Some a
      | Compat(a,b) when same_var a alpha -> Some b
      | Compat(a,b) when same_var b alpha -> Some b
      | _ -> None)

let dep_alphas phi alpha =
  let d = dep_tys alpha phi in
  (* "dep_tys " ^ print_atom alpha ^ " : " ^ print_atoms d |> print_endline; *)
  merge_lists
  @@ List.map (fun t -> let d = ftv_type t in
                (* "ftv " ^ print_atom t ^ " = " ^ print_atoms d |> print_endline; *)
                d)
    d

let deps phi a = merge_lists @@ List.map (dep_alphas phi) a

let deps_eq a d =
  List.for_all (fun alpha -> List.exists (same_var alpha) d) a &&
  List.for_all (fun alpha -> List.exists (same_var alpha) a) d

let rec deps' phi a =
  let d = deps phi a in
  (* "deps " ^ print_atoms a ^ " : " ^ print_atoms d |> print_endline; *)
  if deps_eq a d then a else deps' phi d

let gen_ftv phi gamma tau =
  let ftvenv = ftv_env gamma in
  let ftvtau = ftv_type tau in
  (* "ftv " ^ print_atom tau ^ " : " ^ print_atoms ftvtau |> print_endline; *)
  List.filter
    (fun alpha ->
       let d = deps' phi [alpha] in
       List.filter (fun beta -> List.exists (same_var beta) d) ftvenv = [])
  @@ ftvtau

let gen_vars phi gamma tau =
  let genftv = gen_ftv phi gamma tau in
  (* "gen_ftv : " ^ print_atoms genftv |> print_endline; *)
  [tau] @ merge_lists @@
  List.map (fun alpha ->
      let d = deps' phi [alpha] in
      (* "deps* " ^ print_atom alpha ^ " : " ^ print_atoms d |> print_endline; *)
      d)
  genftv

let in_constr alpha = function
  | Ineq(a,b) when same_var a alpha -> true
  | Ineq(a,b) when same_var b alpha -> true
  | Compat(a,b) when same_var a alpha -> true
  | Compat(a,b) when same_var b alpha -> true
  | _ -> false

let gen phi gamma tau =
  let vars = gen_vars phi gamma tau in
  (* "gen_vars : " ^ print_atoms vars |> print_endline; *)
  let phi_alpha = List.concat @@
    List.map (fun alpha ->
        List.filter (in_constr alpha) phi)
      vars in
  {quant=(Forall (vars,tau));constrs=phi_alpha}
