let print_step = ref false
let print_sigma_update = ref false

let parse_options () =
  let speclist = [
    ("--print-sigma", Arg.Set print_sigma_update, "Enable debug print of sigma addition");
    ("--print-step", Arg.Set print_step, "Enable printing of every step");
    ] in
  Arg.parse speclist (fun _ -> ()) "help"

