open Constraints
open Printer

let fresh, reset =
  let c = ref 0
  and max = ref 10000
  in
  (* ( (function () -> c:=!c+1; if !c >= !max then failwith "No more types";
        (Typevar !c)), *)
  (function () -> c:=!c+1; if !c >= !max then failwith "No more types";
        (Freshvar !c)),
  (function () -> c := 0)


let get_phi, update_local_phi, update_persistent_phi, in_phi, reset_phi =
  (* let phi = ref [Type(Int_type) <= Type(Int_type); Type(Bool_type) <= Type(Bool_type)] in *)
  let local_phi = ref [] in
  let global_phi = ref [] in
  (fun () -> !local_phi @ !global_phi),
  (fun (constr:constr) -> if not (List.mem constr !local_phi) then local_phi := constr::!local_phi),
  (fun constr -> if not (List.mem constr !global_phi) then global_phi := constr::!global_phi),
  (fun constr -> List.mem constr (!local_phi @ !global_phi)),
  (fun () -> local_phi := [])

let make_unnop ty =
  let alpha = fresh () in
  let beta = fresh () in
  let gamma = fresh () in
  let bindings = Forall([alpha; beta; gamma], alpha) in
  let constrs = [beta <= Type(ty); Type(ty) <= gamma; (beta => gamma) <= alpha] in
  {quant=bindings; constrs}

let make_not_null () =
  let alpha = fresh () in
  let beta = fresh () in
  let gamma = fresh () in
  let bindings = Forall([alpha; beta; gamma], alpha) in
  let constrs = [beta <= Nullable(Type(Bool_type)); Nullable(Type(Bool_type)) <= gamma; (beta => gamma) <= alpha] in
  {quant=bindings; constrs}

let make_binop ty =
  let alpha_final = fresh () in
  let alpha_first = fresh () in
  let alpha_second = fresh () in
  let alpha_midfun = fresh () in
  let alpha_result = fresh () in
  let bindings = Forall([alpha_result; alpha_midfun; alpha_second; alpha_first; alpha_final],
                        alpha_final) in
  let constrs = [ alpha_first => alpha_midfun <= alpha_final;
                  alpha_first <= Type(ty);
                  alpha_second => alpha_result <= alpha_midfun;
                  alpha_second <= Type(ty);
                  Type(ty) <= alpha_result] in
  {quant=bindings; constrs}

let make_or () =
  let alpha_final = fresh () in
  let alpha_first = fresh () in
  let alpha_second = fresh () in
  let alpha_midfun = fresh () in
  let alpha_result = fresh () in
  let bindings = Forall([alpha_result; alpha_midfun; alpha_second; alpha_first; alpha_final],
                        alpha_final) in
  let constrs = [ alpha_first => alpha_midfun <= alpha_final;
                  alpha_second => alpha_result <= alpha_midfun;
                  alpha_second <= alpha_result;
                  alpha_second <= alpha_first] in
  {quant=bindings; constrs}

let get_gamma, update_gamma, find_in_gamma, remove_in_gamma =
  let gamma = ref [
    ("not", make_unnop Bool_type);
    ("notnull", make_not_null ());
    ("+", make_binop Int_type);
    ("-", make_binop Int_type);
    ("!-", make_unnop Int_type);
    ("&&", make_binop Bool_type);
    ("||", make_binop Bool_type);
    ("or", make_or ())
    ] in
  let _ = reset () in
  (fun () -> !gamma),
  (fun id data -> gamma := (id, data)::!gamma;
    if !Options.print_sigma_update
    then id ^ " -" ^ print_sigma data |> print_endline),
  (fun id -> List.assoc_opt id !gamma),
  (fun id -> gamma := List.remove_assoc id !gamma)