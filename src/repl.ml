open Alex
open Typer
open Options

let rec do_repl () =
  try
  let rec repl () =
    let lexbuf = Lexing.from_channel stdin in
    print_string "$ ";
    flush stdout;
    let result = Asyn.implementation Alex.main lexbuf in
    (match result with
    | Expr exp -> type_chap exp;
      print_newline(); flush stdout);
    repl ()
    in
  repl ()
  with Failure "type_check" -> print_string "Erreur de typage"; print_newline()
  | Failure s -> print_string ("Erreur " ^ s); print_newline()
  | Parsing.Parse_error -> print_string "Erreur de syntaxe"; print_newline(); do_repl ()

let _ =
  print_endline "Welcome to the REPL. Don't forget to use rlwrap.";
  parse_options ();
  do_repl ()
