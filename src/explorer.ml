open Constraints
open Env
open Printer

let get_compat_with_nil (alpha: atom) =
  let phi = get_phi () in
  let constr =  List.fold_left (fun acc c ->
        (match c with
        | Compat(a, b) when b = alpha && is_nil a -> a
        | Ineq(a, b) when b = alpha && is_nil a -> a
        | _ -> acc)) 
        (Type Bool_type) phi in
  match constr with
  | Type Bool_type -> None
  | _ -> Some constr

let rec replace_atom assoc atom =
  match atom with
  | Typevar(i) as t when List.mem_assoc t assoc -> List.assoc t assoc
  | Freshvar _ as f when List.mem_assoc f assoc -> List.assoc f assoc 
  | Arrow(left, right) -> (replace_atom assoc left) => (replace_atom assoc right)
  | Nullable t -> Nullable (replace_atom assoc t)
  | _ as whatev -> whatev

let replace_constr assoc constr =
  match constr with
  | Ineq(left, right) -> replace_atom assoc left <= replace_atom assoc right
  | FlatIneq(left, right) -> FlatIneq(replace_atom assoc left, replace_atom assoc right)
  | Compat(left, right) -> (replace_atom assoc left) -~ (replace_atom assoc right)
  | FlatCompat(left, right) -> (replace_atom assoc left) =~ (replace_atom assoc right)




(* Resolve for top loop *)

let is_some = function | Some _ -> true | None -> false
let get_opt = function | Some v -> v | None -> raise Not_found

let rec alpha_in_tau alpha tau =
  match tau with
  | Nullable t -> alpha_in_tau alpha t
  | Type _ -> alpha = tau
  | Arrow (left, right) -> alpha_in_tau alpha left || alpha_in_tau alpha right
  | _ -> alpha = tau

let alpha_in_phi alpha =
  let l =  get_phi ()
    |> List.filter (function | Ineq(left, right) when alpha_in_tau alpha left || alpha_in_tau alpha right -> true |_ -> false)
    in
    List.length l <> 0

let in_ineq_constr_ty alpha = function
  | Ineq(a,Type _) when a = alpha -> true
  | Ineq(Type _,b) when b = alpha -> true
  | _ -> false

let in_ineq_constr alpha = function
  | Ineq(a, _) when alpha = a -> true
  | Ineq(_, b) when alpha = b -> true
  | _ -> false

let in_rel c beta =
  let phi = get_phi () in
  phi |> List.exists (function
    | Ineq(a, b) when a = c && b = beta -> true
    | Ineq(b, a) when a = c && b = beta -> true
    | _  -> false)

let in_bineq_constr alpha beta phi =
  let tmp = phi |> List.filter (fun t -> in_ineq_constr alpha t || in_ineq_constr beta t) in
  tmp |> List.filter (function
    | Ineq(a, c) when a = alpha && in_rel c beta -> true
    | Ineq(c, a) when a = alpha && in_rel c beta -> true
    | Ineq(a, c) when alpha = a && c = beta -> true
    | _ -> false)


let rec bilinked_constraints alpha beta =
  let phi = get_phi () in
  in_bineq_constr alpha beta phi

let rec linked_constraints alpha =
  match alpha with
  | Arrow(a, b) -> bilinked_constraints a b @ (bilinked_constraints b a)
  | Nullable a -> linked_constraints a
  | _ -> get_phi () |> List.filter (in_ineq_constr_ty alpha)

let nil_priority t t' =
  match t, t' with
  | Nullable _, _ -> t
  | _, Nullable _ -> t'
  | Arrow _, _ -> t
  | _, Arrow _ -> t'
  | _, _ -> t
  | Type _, _ -> t
  | _, Type _ -> t'

let explore alpha =
  let phi = get_phi () in
  let visited = ref [] in
let rec visit alpha =
  if not @@ List.mem alpha !visited then
    visited := alpha::!visited;
    explore_rec alpha

and explore_rec alpha =
  match alpha with
  | Type _ -> alpha
  | Arrow (left, right) -> Arrow (visit left, visit right)
  | Nullable t -> Nullable (visit t) 
  | _ -> 
  (* let tmp =  *)
  phi
    |> List.map (fun constr -> 
        match constr with 
        | Ineq(b, tau) when alpha = tau && b <> alpha  -> Some (left constr)
        | Ineq(tau, Type _) when alpha = tau -> Some (right constr)
        | _ -> None)
    |> List.filter is_some
    |> List.map get_opt
    |> List.filter (fun alpha -> not @@ List.mem alpha !visited)
    |> List.map (fun alpha -> visit alpha)
    (* in *)
    (* tmp |> List.map print_atom |> concat ";" |> (^) "map:" |> print_endline; *)
    (* tmp *)
    |> List.fold_left (fun elt acc -> nil_priority elt acc) alpha
in
  let final = visit alpha in
  final, linked_constraints final
