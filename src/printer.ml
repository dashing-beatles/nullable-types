open Constraints

let concat = String.concat
let wrap_brackets str = "[" ^ str ^ "]"
let wrap_parens str = "(" ^ str ^ ")"

let ascii i =
  let s = Bytes.create 1 in
   s.[0] <- Char.chr  i;
   s
let var_name n =
  let rec name_of n =
    let q,r = ((n / 26), (n mod 26))
    in
    if q=0 then Bytes.to_string @@ ascii (96+r)
    else (name_of q) ^ Bytes.to_string @@ (ascii (96+r))
  in "'" ^ (name_of n)

let rec dummy () = ()

and string_of_bool = function
  | true -> "true"
  | false -> "false"

and print_const = function
  | Int(i) -> string_of_int i
  | Bool(b) -> string_of_bool b
  | Null -> "null"
  | Unit -> "()"

and print_exp = function
  | Const(const) -> print_const const
  | Var(str) -> str
  | App(f, x) -> print_exp f ^ " " ^ print_exp x
  | IfThenElse(c, t, f) -> "if " ^ print_exp c ^ " then " ^ print_exp t ^ " else " ^ print_exp f
  | Abs(name, body) -> "(\\ " ^ name ^ " -> " ^ print_exp body ^ ")"
  | Let(name, body) -> "let " ^ name ^ " = " ^ print_exp body
  | Case(cond, null, name, not_null) ->
    "case " ^ print_exp cond ^ " of null -> " ^ print_exp null ^
    " | " ^ name ^ " -> " ^ print_exp not_null

and print_bool = function
  | true -> print_string "true"
  | false -> print_string "false"

and print_ml_type = function
  | Int_type -> "int"
  | Bool_type -> "bool"
  | Unit_type -> "unit"

and print_sigma {quant; constrs} =
  (match quant with
  | Forall(l,atom) -> "[forall " ^ (List.map print_atom l |> concat " ") ^ "." ^ print_atom atom)
  ^ " | " ^ (List.map print_constr constrs |> concat " ∧ ") ^ "]"

and print_atom = function
  | Type(ml_type) -> print_ml_type ml_type
  | Typevar(i) -> (*"typevar " ^ *) var_name i
  | Freshvar(i) -> (*"freshvar " ^ *) var_name i
  | Sigma(sigma) -> "sigma_not_implemented"
  | Arrow(left, right) -> print_atom left ^ " -> " ^ print_atom right
  | Nullable(t) -> print_atom t ^ "?"

and print_atoms l = concat ", " @@ List.map print_atom l

and print_constr = function
  | Ineq(left, right) -> print_atom left ^ " ≼ " ^ print_atom right
  | FlatIneq(left, right) -> print_atom left ^ " ≤ " ^ print_atom right
  | Compat(left, right) -> print_atom left ^ " ≈ " ^ print_atom right
  | FlatCompat(left, right) -> print_atom left ^ " ≃ " ^ print_atom right

and print_goal = function
  | Expr(ml_expr, atom) -> print_exp ml_expr ^ " : " ^ print_atom atom
  | Constr(constr) -> print_constr constr
  | Finished -> "⊤"

and print_step goal phi rule =
  print_phi phi ^ " ⊢ " ^ print_goal goal ^ " " ^ wrap_parens rule

and print_phi phi =
  phi |> List.map print_constr |> concat " ∧ " |> wrap_brackets

and print_gamma (gamma: (string * sigma) list) =
  List.map (fun (id, data) -> id ^ ": " ^ print_sigma @@ List.assoc id gamma) gamma 
      |> concat "; " |> wrap_brackets

and print_form = function
  | Alpha a -> "Alpha: " ^ print_atom a
  | Tau t' -> "Tau: " ^ print_atom t'
  | Lit t -> "Simple: " ^ print_atom t
