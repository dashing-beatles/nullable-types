open Constraints
open Printer
open Rules
open Env
open Explorer

exception UnboundVar of string
exception TypeError of string
exception SubtypingError of atom * atom


let get_rule, set_rule =
  let last_rule = ref "start" in
  (fun () -> !last_rule),
  (fun rule -> last_rule := rule)


(* This is solely for LEQVarEnd and GeqVarEnd *)


  
let rec make_rule elts =
  (fun alpha tau_prime ->
    let taus = elts.in_phi alpha in 
    let taus = taus |> List.filter (fun tau -> not(elts.not_in_phi tau_prime tau)) in
      taus |> List.iter (function tau -> update_local_phi @@ elts.add_phi tau tau_prime);
      taus |> List.iter (function tau ->
        set_rule @@ elts.name ^ " (1)";
        type_rec @@ elts.first_goal alpha tau_prime;
        set_rule @@ elts.name ^ "(2)";
        type_rec @@ elts.sec_goal tau tau_prime
        ))


(* Comparison records *)

and leq_var_leq_ty_guard ~alpha ~tau:tau_prime =
  make_guard leq_var_leq_ty_record alpha tau_prime

and leq_var_leq_ty ~alpha ~tau:tau_prime =
  make_rule leq_var_leq_ty_record alpha tau_prime

and geq_var_leq_ty_guard ~alpha ~tau:tau_prime =
  make_guard geq_var_leq_ty_record alpha tau_prime

and geq_var_leq_ty ~alpha ~tau:tau_prime =
  make_rule geq_var_leq_ty_record alpha tau_prime

and leq_ty_leq_var_guard ~alpha ~tau:tau_prime =
  make_guard leq_ty_leq_var_record alpha tau_prime

and leq_ty_leq_var ~alpha ~tau:tau_prime =
  make_rule leq_ty_leq_var_record alpha tau_prime

and geq_ty_leq_var_guard ~alpha ~tau:tau_prime =
  make_guard geq_ty_leq_var_record alpha tau_prime

and geq_ty_leq_var ~alpha ~tau:tau_prime =
  make_rule geq_ty_leq_var_record alpha tau_prime


and leq_var_cpt_ty_guard ~alpha ~tau:tau_prime =
  make_guard leq_var_cpt_ty_record alpha tau_prime

and leq_var_cpt_ty ~alpha ~tau:tau_prime =
  make_rule leq_var_cpt_ty_record alpha tau_prime

and geq_var_cpt_ty_guard ~alpha ~tau:tau_prime =
  make_guard geq_var_cpt_ty_record alpha tau_prime

and geq_var_cpt_ty ~alpha ~tau:tau_prime =
  make_rule geq_var_cpt_ty_record alpha tau_prime

(* Compat functions *)

and cpt_var_leq_ty_guard ~alpha ~tau:tau_prime =
  make_guard cpt_var_leq_ty_record alpha tau_prime

and cpt_var_leq_ty ~alpha ~tau:tau_prime =
  make_rule cpt_var_leq_ty_record alpha tau_prime

and cpt_ty_leq_var_guard ~alpha ~tau:tau_prime =
  make_guard cpt_ty_leq_var_record alpha tau_prime

and cpt_ty_leq_var ~alpha ~tau:tau_prime =
  make_rule cpt_ty_leq_var_record alpha tau_prime

and cpt_var_cpt_ty_guard ~alpha ~tau:tau_prime =
  make_guard cpt_var_cpt_ty_record alpha tau_prime

and cpt_var_cpt_ty ~alpha ~tau:tau_prime =
  make_rule cpt_var_cpt_ty_record alpha tau_prime

(* Back to regular functions *)


and type_rec goal =
  if !Options.print_step then
  print_step goal (get_phi ()) (get_rule()) |> print_endline;
  (* ignore @@ read_line (); *)
  match goal with
  | Expr(expr, atom) -> type_exp expr atom
  | Constr(constr) -> type_constr constr
  | Finished -> ()

and type_exp exp atom =
  (* get_gamma () |> print_gamma |> print_endline; *)
  match exp with
  | Const(term) -> type_term term atom
  | App(f, x) -> type_app f x atom
  | Var(str) -> type_var str atom
  | IfThenElse(cond, tru, fals) -> type_if_then_else cond tru fals atom
  | Abs(name, body) -> type_lambda name body atom
  | Let(name, body) -> type_let name body atom
  | Case(cond, null, name, notnull) -> type_case cond null name notnull atom
  | _ -> assert false

and type_case cond null name notnull atom =
  let alpha = fresh () in
  let beta = fresh () in
  set_rule "TCase (1)";
  update_persistent_phi @@ make_ineq alpha beta;
  type_rec @@ Expr(cond, beta);
  set_rule "TCase (2)";
  type_rec @@ Expr(null, atom);
  set_rule "TCase (3)";
  update_gamma name {quant=Forall([], alpha); constrs=[]};
  type_rec @@ Expr(notnull, atom);

and type_let name body atom =
  let alpha = fresh () in
  set_rule "TLet (1)";
  type_rec @@ Expr(body, alpha);
  let added = (Gen.gen (get_phi ()) (get_gamma ()) alpha) in
  update_gamma name added

and type_lambda name body atom =
  let alpha = fresh () in
  let beta = fresh () in
  let bindings = Forall([], alpha) in
  let constrs = [] in
  update_gamma name {quant=bindings; constrs};
  set_rule "TLambda (1)";
  type_rec @@ Expr(body, beta);
  remove_in_gamma name;
  set_rule "TLambda (2)";
  type_rec @@ make_ineq_goal (alpha => beta) atom;

and type_if_then_else cond texp fexp atom =
  set_rule "TIfThenElse (1)";
  type_rec @@ Expr(cond, (Type Bool_type));
  set_rule "TIfThenElse (2)";
  type_rec @@ Expr(texp, atom);
  set_rule "TIfThenElse (3)";
  type_rec @@ Expr(fexp, atom)



(* TInst *)
and type_var str atom =
  set_rule "TInst";
  match find_in_gamma str with
  | None -> raise @@ UnboundVar str
  | Some(sigma) -> inst sigma atom

and inst {quant=Forall(bindings, bindee); constrs} atom =
  let assoc = bindings |> List.map (fun elt -> (elt, fresh ())) in
  let bindee = match List.length assoc with
    | 0 -> bindee
    | _ -> List.assoc bindee assoc in
  constrs
    |> List.map (replace_constr assoc)
    |> List.iter update_persistent_phi;
  type_rec @@ make_ineq_goal bindee atom
  

(* TApp *)
and type_app f x atom =
  let alpha = fresh () in
  set_rule "TApp (1)";
  type_rec (Expr(f, alpha => atom));
  set_rule "TApp (2)";
  type_rec (Expr(x, alpha))


and type_term term atom =
  match term with
  | Int _ ->
      set_rule "TConst";
      type_rec @@ make_ineq_goal (Type Int_type) atom
  | Bool _ -> 
      set_rule "TConst";
      type_rec @@ make_ineq_goal (Type Bool_type) atom
  | Null -> 
      set_rule "TNull";
      let alpha = fresh () in
      type_rec @@ make_ineq_goal (Nullable alpha) atom
  | Unit ->
      set_rule "TConst";
      type_rec @@ make_ineq_goal (Type Unit_type) atom
  | _ -> assert false

and type_constr constr =
  match constr with
  | Ineq(left, right) as ineq -> type_ineq ineq 
  | FlatIneq(left, right) as flatineq -> type_flatineq left right
  | FlatCompat(left, right) as flat_compat -> type_flatcompat left right
  | Compat(left, right) as compat -> type_compat compat

and type_ineq (Ineq(left, right) as ineq) =
  match (in_phi ineq) with
  (* LEQNew *)
  | false ->
      set_rule "LEQNew";
      update_persistent_phi ineq;
      type_rec @@ make_flatineq_goal left right 
  | true -> 
      set_rule "LeqAlreadyProved";
      type_rec @@ Finished

and type_compat (Compat(left, right) as compat) =
  match (in_phi compat) with
  | false ->
      set_rule "CPTNew";
      update_persistent_phi compat;
      type_rec @@ make_flatcompat_goal left right
  | true ->
      set_rule "CPTAlreadyProved";
      type_rec Finished

and formalize atom =
  match atom with
  | Type(_) as t -> Tau t
  | Typevar _ as a -> Alpha a
  | Freshvar _ as a -> Alpha a
  | Arrow (_,_) as arrow  -> Tau arrow
  | Nullable _ as t -> Tau t
  | _ -> assert false

and type_flatineq left right =
  (* print_form @@ formalize left |> print_endline;
  print_form @@ formalize right |> print_endline; *)
  match (formalize left, formalize right) with
  | Tau(Nullable a), Tau(Nullable b) -> 
        set_rule "Invented (1)";
        type_rec @@ make_ineq_goal a b;
        set_rule "Invented (2)";
        type_rec @@ make_ineq_goal b a;
  | Tau(Type _), Tau(Type _) when left = right ->
        set_rule "LeqBaseTy";
        type_rec Finished
  | Tau(Type _), Tau(Nullable(t)) when left = t ->
        set_rule "LeqBaseNull";
        type_rec Finished
  | Tau(Arrow(tau_1, tau_2)), Tau(Arrow(tau_a, tau_b)) -> 
        set_rule "LeqArrow (1)";
        type_rec @@ make_ineq_goal tau_a tau_1;
        set_rule "LeqArrow (2)";
        type_rec @@ make_ineq_goal tau_2 tau_b
  | Alpha a, Alpha b when a = b ->
        set_rule "LeqSameVar";
        type_rec Finished
  | Alpha a, _ when leq_var_leq_ty_guard ~alpha:a ~tau:right ->
        leq_var_leq_ty ~alpha:a ~tau:right
  | Alpha a, _ when leq_ty_leq_var_guard ~alpha:a ~tau:right ->
        leq_ty_leq_var ~alpha:a  ~tau:right
  | Alpha a, _ when leq_var_cpt_ty_guard ~alpha:a ~tau:right ->
        leq_var_cpt_ty ~alpha:a ~tau:right
  | _, Alpha a when geq_var_leq_ty_guard ~alpha:a ~tau:left ->
        geq_var_leq_ty ~alpha:a ~tau:left
  | _, Alpha a when geq_ty_leq_var_guard ~alpha:a ~tau:left ->
        geq_ty_leq_var ~alpha:a ~tau:left
  | _, Alpha a when geq_var_cpt_ty_guard ~alpha:a ~tau:left ->
        geq_var_cpt_ty ~alpha:a ~tau:left
  | Alpha a, _ when leq_var_end ~alpha:a ~tau:right ->
        set_rule "LeqVarEnd";
        type_rec Finished;
  | _, Alpha a when geq_var_end ~tau:left ~alpha:a ->
        set_rule "GeqVarEnd";
        type_rec Finished
  | _ ->
      raise @@ SubtypingError (left, right)

and new_rule ~simple_ty ~alpha ~null =
      update_local_phi @@ (simple_ty -~ null);
      set_rule "TestRule (1)";
      type_rec @@ make_ineq_goal alpha simple_ty;
      set_rule "TestRule (2)";
      type_rec @@ make_ineq_goal simple_ty alpha

and type_flatcompat left right =
  (* print_form @@ formalize left |> print_endline;
  print_form @@ formalize right |> print_endline; *)
  match (formalize left, formalize right) with
  | Tau(Nullable alpha), Tau(Type _) ->
      new_rule ~simple_ty:right ~alpha:alpha ~null:left
  | Tau(Type _), Tau(Nullable alpha) ->
      new_rule ~simple_ty:left ~alpha:alpha ~null:right
  | Tau(Type _), Tau(Type _) when left = right ->
      set_rule "CptBaseTy";
      type_rec Finished
  | Tau(Type _), Tau(Nullable(t)) when left = t ->
      set_rule "CptBaseNull";
      type_rec Finished
  | Tau(Nullable _), Tau(Nullable _) when left = right ->
      set_rule "CptSameVar";
      type_rec Finished
  | Tau(Arrow(tau_1, tau_2)), Tau(Arrow(tau_a, tau_b)) -> 
        set_rule "CptArrow (1)";
        type_rec @@ make_compat_goal tau_1 tau_a;
        set_rule "CptArrow (2)";
        type_rec @@ make_compat_goal tau_2 tau_b;
  | Tau(Arrow(tau_1, tau_2)), Tau(Nullable(Arrow(tau_a, tau_b)))
  | Tau(Nullable(Arrow(tau_1, tau_2))), Tau(Arrow(tau_a, tau_b)) ->
        set_rule "CptArrowNull (1)";
        type_rec @@ make_compat_goal tau_1 tau_a;
        set_rule "CptArrowNull (2)";
        type_rec @@ make_compat_goal tau_2 tau_b
  | Alpha a, Alpha b when a = b ->
        set_rule "CptSameVar";
        type_rec Finished
  | Alpha a, _ when cpt_var_leq_ty_guard ~alpha:a ~tau:right ->
        cpt_var_leq_ty ~alpha:a ~tau:right
  | Alpha a, _ when cpt_ty_leq_var_guard ~alpha:a ~tau:right ->
        cpt_ty_leq_var ~alpha:a ~tau:right
  | Alpha a, _ when cpt_var_cpt_ty_guard ~alpha:a ~tau:right ->
        cpt_var_cpt_ty ~alpha:a ~tau:right
  | Alpha a, _ when cpt_var_end ~alpha:a ~tau:right ->
        set_rule "CptVarEnd";
        type_rec Finished
  | _, Alpha a when cpt_var_leq_ty_guard ~alpha:a ~tau:left ->
        cpt_var_leq_ty ~alpha:a ~tau:left
  | _, Alpha a when cpt_ty_leq_var_guard ~alpha:a ~tau:left ->
        cpt_ty_leq_var ~alpha:a ~tau:left
  | _, Alpha a when cpt_var_cpt_ty_guard ~alpha:a ~tau:left ->
        cpt_var_cpt_ty ~alpha:a ~tau:left
  | _, Alpha a when cpt_var_end ~alpha:a ~tau:left ->
        set_rule "CptVarEnd";
        type_rec @@ Finished
  | _ -> 
        let msg = "Can't unify " ^ print_atom left ^ " and " ^ print_atom right in
        raise (TypeError msg)

let type_chap exp =
  try
  set_rule "start";
  let tau = fresh () in
  type_rec (Expr (exp, (tau)));
  let tau_str = if alpha_in_phi tau then
    let {quant=Forall (vars,t)
        ;constrs} = Gen.gen (get_phi()) (get_gamma ()) tau in
    " ∀ " ^ print_atoms vars ^
    " - " ^ print_atom t ^ " | " ^
    (constrs |> List.map print_constr |> concat " ∧ ")
    else " - unit"
  in
  reset_phi(); get_phi () |> print_phi |> print_endline;
  tau_str |> print_endline;

  with
  | TypeError msg -> "Typing error: " ^ msg |> print_endline
  | UnboundVar x -> "Unbound variable: " ^ x |> print_endline
  | SubtypingError(left, right) ->
      "No subtyping between " ^ print_atom left ^ " and " ^ print_atom right |> print_endline
  | NilError atom -> print_atom atom ^ " is not a nullable type." |> print_endline

(* let _ =
  Printexc.record_backtrace true *)
