type ml_expr =
  | Const of ml_const
  |  Var of string
  |  Pair of ml_expr * ml_expr
  |  Cons of ml_expr * ml_expr
  |  IfThenElse of ml_expr * ml_expr * ml_expr
  |  Case of ml_expr * ml_expr * string * ml_expr
  |  App of ml_expr * ml_expr
  |  Abs of string * ml_expr
  |  Ref of ml_expr
  | Let of string * ml_expr

and ml_const =
  | Int of int
  | Float of float
  | Null
  | Unit
  | Bool of bool
  (* | Emptylist *)

type ml_phrase =
  |  Expr of ml_expr


type quantified_type =
  Forall of (atom list) * atom

and sigma = {quant: quantified_type; constrs: constr list}

and ml_type =
  | Int_type
  | Bool_type
  | Unit_type

and atom =
  | Type of ml_type
  | Typevar of int
  | Freshvar of int
  | Arrow of atom * atom
  | Sigma of sigma
  | Nullable of atom

and constr =
  | Ineq of atom * atom
  | FlatIneq of atom * atom
  | Compat of atom * atom
  | FlatCompat of atom * atom

and goal =
  | Expr of ml_expr * atom
  | Constr of constr
  | Finished

type formalism =
  | Alpha of atom
  | Tau of atom
  | Lit of atom

let (<=) alpha beta =
  Ineq(alpha, beta)

let (=>) alpha beta =
  Arrow(alpha, beta)

let (-~) alpha beta =
  Compat(alpha, beta)

let (=~) alpha beta =
  FlatCompat(alpha, beta)

let make_ineq_goal left right =
  Constr(Ineq(left, right))

let make_flatineq_goal left right =
  Constr(FlatIneq(left, right))

let make_compat_goal left right =
  Constr(Compat(left, right))

let make_flatcompat_goal left right =
  Constr(FlatCompat(left, right))

let make_ineq left right =
  Ineq(left, right)

let make_flatineq left right =
  FlatIneq(left, right)

let make_compat left right =
  Compat(left, right)

let left, right =
  (function | FlatIneq(left, right) -> left | Ineq(left, right) -> left | Compat(left, right) -> left),
  (function | FlatIneq(left, right) -> right| Ineq(left, right) -> right | Compat(left, right) -> right)

let is_nil = function
  | Nullable _ -> true
  | _ -> false

exception NilError of atom
let get_nil alpha =
  match alpha with
  | Nullable a -> a
  | _ -> raise @@ NilError alpha