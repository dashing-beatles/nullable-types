open Constraints
open Env

type rule_elt = {
  in_phi: atom -> atom list;
  not_in_phi: atom -> atom -> bool;
  add_phi: atom -> atom -> constr;
  first_goal: (atom -> atom -> goal);
  sec_goal: (atom -> atom -> goal);
  name: string
  }
let get_phi_alpha_leq_tau alpha =
  let phi = get_phi () in
  phi |> List.filter (function | Ineq(left, right) when alpha = left -> true | _ -> false)
      |> List.map (function | Ineq(left, right) -> right | _ -> assert false)

let get_phi_tau_leq_alpha alpha =
  get_phi () |> List.filter (fun c -> match c with | Ineq(left, right) when alpha = right -> true | _ -> false)
             |> List.map (function | Ineq(left, right) -> left | _ -> assert false)

let get_phi_alpha_eq_tau alpha =
  get_phi () |> List.filter (function | Compat(left, right) when alpha = right || alpha = left -> true | _ -> false)
             |> List.map (function | Compat(left, right) when alpha = right -> right
                                   | Compat(left, right) when alpha = left -> left
                                   | _ -> assert false)

let equivalent_in_phi tau_prime =
  fun tau ->
    (* "equiv:" ^ print_atom tau ^ " | " ^ print_atom tau_prime |> print_endline; *)
  let ret = in_phi (Compat (tau, tau_prime)) || in_phi (Compat (tau_prime, tau)) (*|| tau == tau_prime*) in
  ret


let leq_in_phi_simple tau_prime =
  fun tau -> in_phi (Ineq(tau, tau_prime))

let leq_in_phi_prime tau_prime =
  fun tau -> in_phi (Ineq(tau_prime, tau))

let forall_equiv tau_prime l =
  List.for_all (equivalent_in_phi tau_prime) l

let forall_leq tau_prime l =
  List.for_all (leq_in_phi_simple tau_prime) l

let forall_leq_prime tau_prime l =
  List.for_all (leq_in_phi_prime tau_prime) l

let leq_var_leq_ty_record =
  {
    in_phi = get_phi_alpha_leq_tau;
    not_in_phi = equivalent_in_phi;
    add_phi = make_compat;
    first_goal = make_flatineq_goal;
    sec_goal = make_flatcompat_goal;
    name = "LeqVarLeqTy"
  }

and geq_var_leq_ty_record = {
  in_phi = get_phi_alpha_leq_tau;
  not_in_phi = leq_in_phi_prime;
  add_phi = (fun tau tau_prime -> make_ineq tau_prime tau);
  first_goal = (fun alpha tau_prime -> make_flatineq_goal tau_prime alpha);
  sec_goal = (fun tau tau' -> make_flatineq_goal tau' tau);
  name = "GeqVarLeqTy"
}

and leq_ty_leq_var_record = {
  in_phi = get_phi_tau_leq_alpha;
  not_in_phi = leq_in_phi_simple;
  add_phi = make_ineq;
  first_goal = make_flatineq_goal;
  sec_goal = make_flatineq_goal;
  name = "LeqTyLeqVar";
}

and geq_ty_leq_var_record = {
  in_phi = get_phi_tau_leq_alpha;
  not_in_phi = equivalent_in_phi;
  add_phi = make_compat;
  first_goal = (fun alpha tau' -> make_flatineq_goal tau' alpha);
  sec_goal = make_flatcompat_goal;
  name = "GeqTyLeqVar"
}

and leq_var_cpt_ty_record = {
  in_phi = get_phi_alpha_eq_tau;
  not_in_phi = equivalent_in_phi;
  add_phi = make_compat;
  first_goal = make_flatineq_goal;
  sec_goal = make_flatcompat_goal;
  name = "LeqVarCptTy";
}

and geq_var_cpt_ty_record = {
  in_phi = get_phi_alpha_eq_tau;
  not_in_phi = equivalent_in_phi;
  add_phi = make_compat;
  first_goal = (fun alpha tau' -> make_flatineq_goal tau' alpha);
  sec_goal = make_flatcompat_goal;
  name = "GeqVarCptTy";
}

(* Cpt records *)

and cpt_var_leq_ty_record = {
  in_phi = get_phi_alpha_leq_tau;
  not_in_phi = equivalent_in_phi;
  add_phi = make_compat;
  first_goal = make_flatcompat_goal;
  sec_goal = make_flatcompat_goal;
  name = "CptVarLeqTy";
}

and cpt_ty_leq_var_record = {
  in_phi = get_phi_tau_leq_alpha;
  not_in_phi = equivalent_in_phi;
  add_phi = make_compat;
  first_goal = make_flatcompat_goal;
  sec_goal = make_flatcompat_goal;
  name = "CptTyLeqVar";
}

and cpt_var_cpt_ty_record = {
  in_phi = get_phi_alpha_eq_tau;
  not_in_phi = equivalent_in_phi;
  add_phi = make_compat;
  first_goal = make_flatcompat_goal;
  sec_goal = make_flatcompat_goal;
  name = "CptVarCptTy";
}
let cpt_var_end ~alpha ~tau:tau' =
  let one = (get_phi_alpha_leq_tau alpha |> forall_equiv tau') in
  let two = (get_phi_tau_leq_alpha alpha |> forall_equiv tau') in
  let three = (get_phi_alpha_eq_tau alpha |> forall_equiv tau') in
  (* print_bool one;
  print_bool two;
  print_bool three; *)
  (alpha <> tau') && one && two && three


let leq_var_end ~alpha ~tau:tau_prime =
  let one = (get_phi_alpha_leq_tau alpha |> forall_equiv tau_prime) in
  let two = (get_phi_tau_leq_alpha alpha |> forall_leq tau_prime) in
  let three = (get_phi_alpha_eq_tau alpha |> forall_equiv tau_prime) in
  (* print_bool one;
  print_bool two;
  print_bool three; *)
  (alpha <> tau_prime) && one && two && three

let geq_var_end ~alpha ~tau:tau_prime =
  let one = (get_phi_alpha_leq_tau alpha |> forall_leq_prime tau_prime) in
  let two = (get_phi_tau_leq_alpha alpha |> forall_equiv tau_prime) in
  let three = (get_phi_alpha_eq_tau alpha |> forall_equiv tau_prime) in
  (* get_phi_alpha_leq_tau alpha |> List.map print_atom |> concat ";" |> print_endline;
  print_bool one;
  print_bool two;
  print_bool three; *)
  (alpha <> tau_prime) && one && two && three
and make_guard elts alpha tau_prime =
  let taus = elts.in_phi alpha |> List.filter (fun tau -> not(elts.not_in_phi tau_prime tau)) in
  alpha <> tau_prime && List.length taus <> 0